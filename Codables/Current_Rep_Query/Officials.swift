//
//  Officials.swift
//  Representatives
//
//  Created by Zachary Steinberg on 11/10/19.
//  Copyright © 2019 Zachary Steinberg. All rights reserved.
//

import Foundation

struct Officials: Codable {
    let name: String
    let party: String
    let phones: [String]?
    let urls: [URL]?
    let emails: [String]?
}
