//
//  Address.swift
//  Representatives
//
//  Created by Zachary Steinberg on 11/10/19.
//  Copyright © 2019 Zachary Steinberg. All rights reserved.
//

import Foundation

struct Address: Codable {
    let line1: String
    let line2: String?
    let line3: String?
    let city: String
    let state: String
    let zip: String
}
