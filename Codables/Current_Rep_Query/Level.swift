//
//  Level.swift
//  Representatives
//
//  Created by Zachary Steinberg on 11/10/19.
//  Copyright © 2019 Zachary Steinberg. All rights reserved.
//

import Foundation

struct Level: Codable {
    let name: String
    let officeIndices: [Int]?
}
