//
//  api_results.swift
//  Elections
//
//  Created by Zachary Steinberg on 11/12/19.
//  Copyright © 2019 Zachary Steinberg. All rights reserved.
//

import Foundation

struct api_results: Codable {
    let kind: String
    let election: Election
    let normalizedInput: normAddress
//    let contests: [Contest]
    let state: [State]
}
