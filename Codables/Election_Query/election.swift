//
//  election.swift
//  Elections
//
//  Created by Zachary Steinberg on 11/13/19.
//  Copyright © 2019 Zachary Steinberg. All rights reserved.
//

import Foundation

struct Election: Codable {
    let id: String
    let name: String
    let electionDay: String
    let ocdDivisionId: String
}
