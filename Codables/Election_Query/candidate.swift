//
//  candidate.swift
//  Elections
//
//  Created by Zachary Steinberg on 11/13/19.
//  Copyright © 2019 Zachary Steinberg. All rights reserved.
//

import Foundation

struct Candidate: Codable {
    let name: String
    let party: String
    let candidateUrl: String
    let phone: String
    let photoUrl: String?
    let email: String
    let orderOnBallot: Int?
    let channels: [socialMedia]
    
    
}
