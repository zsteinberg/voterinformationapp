//
//  Contest.swift
//  Elections
//
//  Created by Zachary Steinberg on 11/13/19.
//  Copyright © 2019 Zachary Steinberg. All rights reserved.
//

import Foundation

//Need to do further work. Can be type General, Primary, Run Off, or Referendum
//Referendum has different fields

struct Contest: Codable {
    let type: String
    let office: String?
    let level: [String]?
    let roles: [String]?
    let district: District
    let candidates: [Candidate]
    let sources: [Source]
}
