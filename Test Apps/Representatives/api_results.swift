//
//  api_results.swift
//  Representatives
//
//  Created by Zachary Steinberg on 11/10/19.
//  Copyright © 2019 Zachary Steinberg. All rights reserved.
//

import Foundation

struct api_results: Codable {
    let kind: String
    let normalizedInput: normAddress
    let divisions: [String:Level]
    let offices: [Office]
    let officials: [Officials]
}
