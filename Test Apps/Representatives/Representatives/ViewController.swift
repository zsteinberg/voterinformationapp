//
//  ViewController.swift
//  Representatives
//
//  Created by Zachary Steinberg on 11/6/19.
//  Copyright © 2019 Zachary Steinberg. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UISearchBarDelegate {
    @IBOutlet weak var searchBar: UISearchBar!
    
    @IBOutlet weak var apiLabel: UILabel!
    
    var api_key: String = "AIzaSyDR2SZV7sv5g5rcRPNjVFGo-CL6y-d_LhY"
    var address_query: String = "https://www.googleapis.com/civicinfo/v2/representatives?address="
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchBar.delegate = self
        
        // Do any additional setup after loading the view.
    }
    
    func queryAPI(query: String){
        let newQuery = query.replacingOccurrences(of: " ", with: "+")
        self.apiLabel.text = self.address_query+newQuery+"&key=\(self.api_key)"
        DispatchQueue.global(qos: .userInitiated).async {
            
            let url = URL(string: self.address_query+newQuery+"&key=\(self.api_key)")
            let data = try! Data(contentsOf: url!)
            let json = try! JSONDecoder().decode(api_results.self, from: data)
            print(json)
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        queryAPI(query: searchBar.text!)
    }


}

