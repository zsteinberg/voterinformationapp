//
//  Office.swift
//  Representatives
//
//  Created by Zachary Steinberg on 11/10/19.
//  Copyright © 2019 Zachary Steinberg. All rights reserved.
//

import Foundation

struct Office: Codable{
    let name: String
    let divisionId: String
    let levels: [String]?
    let roles: [String]?
    let officialIndices: [Int]
}
