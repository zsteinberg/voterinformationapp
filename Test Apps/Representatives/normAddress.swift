//
//  normAddress.swift
//  Representatives
//
//  Created by Zachary Steinberg on 11/10/19.
//  Copyright © 2019 Zachary Steinberg. All rights reserved.
//

import Foundation

struct normAddress: Codable {
    let line1: String
    let city: String
    let state: String
    let zip: String
}
