//
//  source.swift
//  layout
//
//  Created by Zachary Steinberg on 12/2/19.
//  Copyright © 2019 Sami Klein. All rights reserved.
//

import Foundation

struct Source: Codable {
    let name: String?
    let office: Bool?
}

