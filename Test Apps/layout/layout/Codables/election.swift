//
//  election.swift
//  layout
//
//  Created by Zachary Steinberg on 12/2/19.
//  Copyright © 2019 Sami Klein. All rights reserved.
//

import Foundation

struct Election: Codable {
    let id: String
    let name: String
    let electionDay: String
    let ocdDivisionId: String
}
