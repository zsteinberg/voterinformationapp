//
//  poll.swift
//  layout
//
//  Created by Bailey Winston on 11/21/19.
//  Copyright © 2019 Sami Klein. All rights reserved.
//

import Foundation

struct poll: Codable {
    
    //let id: String
    let address: normAddress
    let notes: String
    let pollingHours: String
    //let name: String
    //let voterServices: String
    //let startDate: String
    //let endDate: String
    let sources: [Source]
}
