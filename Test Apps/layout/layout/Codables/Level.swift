//
//  Level.swift
//  layout
//
//  Created by Zachary Steinberg on 11/18/19.
//  Copyright © 2019 Sami Klein. All rights reserved.
//

import Foundation

struct Level: Codable {
    let name: String
    let officeIndices: [Int]?
}
