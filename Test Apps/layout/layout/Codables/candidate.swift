//
//  candidate.swift
//  layout
//
//  Created by Zachary Steinberg on 11/29/19.
//  Copyright © 2019 Sami Klein. All rights reserved.
//

import Foundation

struct Candidate: Codable {
    let name: String
    let party: String?
    let candidateUrl: String?
    let phone: String?
    let email: String?
    let channels: [Channel]?
    
}
