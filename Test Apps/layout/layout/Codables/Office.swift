//
//  Office.swift
//  layout
//
//  Created by Zachary Steinberg on 11/18/19.
//  Copyright © 2019 Sami Klein. All rights reserved.
//

import Foundation

struct Office: Codable{
    let name: String
    let divisionId: String
    let levels: [String]?
    let roles: [String]?
    let officialIndices: [Int]
}
