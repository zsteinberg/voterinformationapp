//
//  infoSource.swift
//  layout
//
//  Created by Zachary Steinberg on 11/29/19.
//  Copyright © 2019 Sami Klein. All rights reserved.
//

import Foundation

struct infoSource: Codable {
    let name: String
    let official: Bool
}
