//
//  state.swift
//  layout
//
//  Created by Zachary Steinberg on 12/2/19.
//  Copyright © 2019 Sami Klein. All rights reserved.
//

import Foundation

struct State: Codable {
    let name: String
    let electionAdministrationBody: adminBody
    let local_jurisdiction: jurisdiction?
    let sources: [Source]
}
