//
//  normAddress.swift
//  layout
//
//  Created by Zachary Steinberg on 11/18/19.
//  Copyright © 2019 Sami Klein. All rights reserved.
//

import Foundation

struct normAddress: Codable {
    let line1: String
    let line2: String?
    let line3: String?
    let city: String
    let state: String
    let zip: String
}
