//
//  official.swift
//  layout
//
//  Created by Zachary Steinberg on 12/2/19.
//  Copyright © 2019 Sami Klein. All rights reserved.
//

import Foundation

struct Official: Codable {
    let name: String?
    let title: String?
    let officePhoneNumber: String
    let faxNumber: String?
    let emailAddress: String?
}
