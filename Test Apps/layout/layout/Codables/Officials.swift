//
//  Officials.swift
//  layout
//
//  Created by Zachary Steinberg on 11/18/19.
//  Copyright © 2019 Sami Klein. All rights reserved.
//

import Foundation

struct Officials: Codable {
    let name: String
    let party: String
    let phones: [String]?
    let urls: [URL]?
    let photoUrl: URL?
    let emails: [String]?
}
