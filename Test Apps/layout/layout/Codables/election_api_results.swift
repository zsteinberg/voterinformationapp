//
//  api_results.swift
//  Elections
//
//  Created by Zachary Steinberg on 11/12/19.
//  Copyright © 2019 Zachary Steinberg. All rights reserved.
//

import Foundation

struct election_api_results: Codable {
    let kind: String
    let election: Election
    let normalizedInput: normAddress
    let contests: [Contest]
    let pollingLocations: [poll]?
    let state: [State]
}
