//
//  contest.swift
//  layout
//
//  Created by Zachary Steinberg on 11/29/19.
//  Copyright © 2019 Sami Klein. All rights reserved.
//

import Foundation

struct Contest: Codable {
    let type: String
    let office: String?
    let level: [String]?
    let roles: [String]?
    let district: District?
    let referendumTitle: String?
    let referendumSubtitle: String?
    let referendumUrl: String?
    let candidates: [Candidate]?
    let sources: [infoSource]?
}
