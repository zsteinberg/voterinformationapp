//
//  api_results.swift
//  layout
//
//  Created by Zachary Steinberg on 11/18/19.
//  Copyright © 2019 Sami Klein. All rights reserved.
//

import Foundation

struct api_results: Codable {
    let kind: String
    let normalizedInput: normAddress
    let divisions: [String:Level]
    let offices: [Office]
    let officials: [Officials]
}
