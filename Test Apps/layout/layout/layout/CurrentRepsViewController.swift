//
//  CurrentRepsViewController.swift
//  layout
//
//  Created by Kristen Lau on 11/17/19.
//  Copyright © 2019 Sami Klein. All rights reserved.
//

import UIKit

class CurrentRepsCell: UITableViewCell {
    @IBOutlet weak var repPhoto: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var office: UILabel!
}



class CurrentRepsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    var apiKey: String = "&key=AIzaSyDR2SZV7sv5g5rcRPNjVFGo-CL6y-d_LhY"
    var query: String = "https://www.googleapis.com/civicinfo/v2/representatives?address="
    var queryURL: URL?
    var apiResults: api_results?
    var reps: [Officials] = []
    var officeIndices: [String: [Int]] = [:]
    
    @IBOutlet weak var currentReps: UITableView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reps.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
//        let choooseRepVC = DetailedCandidateViewController()
        print(reps[indexPath.row])
        let rep = reps[indexPath.row]
        
        let nextController = (self.storyboard?.instantiateViewController(withIdentifier: "chosenRepVC") as? DetailedCandidateViewController)!
        
        
        
        if rep.photoUrl != nil{
            nextController.repPhotoURL = rep.photoUrl
        } else {
            nextController.repPhotoURL = nil
        }
        
        print(rep.phones ?? "NONE")
        if rep.phones != nil{
            nextController.repPhone = rep.phones![0]
        } else {
            nextController.repPhone = "No Phone Number Available"
        }
        if rep.emails != nil{
            nextController.repEmailAddress = rep.emails![0]
        } else {
            nextController.repEmailAddress = "No Email Address Available"
        }
        nextController.repParty = rep.party
        nextController.repName = rep.name
        if rep.urls != nil{
            nextController.webURL = rep.urls![0]
        } else {
            nextController.webURL = nil
        }
        nextController.cameFromCurrentReps = true
        
        self.show(nextController, sender: self.storyboard)
        
//        navigationController?.pushViewController(choooseRepVC, animated: true)
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "repCell")! as! CurrentRepsCell

        cell.name?.text = reps[indexPath.row].name
        for office in officeIndices{
            if office.value.contains(indexPath.row){
                cell.office.text = office.key
            }
        }
        if reps[indexPath.row].photoUrl != nil {
            let data = try? Data(contentsOf: reps[indexPath.row].photoUrl!)
            cell.repPhoto?.image = UIImage(data: data!)
        } else {
            let noPosterAvail = #imageLiteral(resourceName: "NoImageAvail")
            cell.repPhoto?.image = noPosterAvail
        }
        
        return cell
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        currentReps.delegate = self
        currentReps.dataSource = self
        queryAPI()
        
    }
    
    func queryAPI(){
//        self.apiLabel.text = self.address_query+newQuery+"&key=\(self.api_key)"
        queryURL = URL(string: query+MyVariables.address!+apiKey)
        spinner.startAnimating()
        DispatchQueue.global(qos: .userInitiated).async {
            do{
                let data = try! Data(contentsOf: self.queryURL!)
                self.apiResults = try! JSONDecoder().decode(api_results.self, from: data)
                self.reps = self.apiResults!.officials
                for office in self.apiResults!.offices {
                    self.officeIndices[office.name] = office.officialIndices
                    DispatchQueue.main.async {
                        self.currentReps.reloadData()
                        self.spinner.stopAnimating()
                        //self.spinner.isHidden = true
                    }
                }

            } catch let parsingError{
                print("Error")
            }

        }

    }
    }
    


