//
//  MenuViewController.swift
//  layout
//
//  Created by Kristen Lau on 11/18/19.
//  Copyright © 2019 Sami Klein. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController {

    @IBOutlet weak var stateLevel: UIButton!
    @IBOutlet weak var localLevel: UIButton!
    @IBOutlet weak var button: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        if MyVariables.elect_api_results?.state[0].electionAdministrationBody == nil {
            stateLevel.isHidden = true
        }
        if MyVariables.elect_api_results?.state[0].local_jurisdiction == nil {
            localLevel.isHidden = true
        }
        // Do any additional setup after loading the view.
    }
    
    @IBAction func goBack(_ sender: Any) {
        let transition: CATransition = CATransition()
        transition.duration = 0.3
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.reveal
        transition.subtype = CATransitionSubtype.fromRight
        self.view.window!.layer.add(transition, forKey: nil)
        self.dismiss(animated: false, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
