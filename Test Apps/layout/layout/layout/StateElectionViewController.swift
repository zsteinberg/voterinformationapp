//
//  StateElectionViewController.swift
//  layout
//
//  Created by Zachary Steinberg on 12/1/19.
//  Copyright © 2019 Sami Klein. All rights reserved.
//

import UIKit

class StateElectionViewController: UIViewController {
    
    var adminBody: adminBody = MyVariables.elect_api_results!.state[0].electionAdministrationBody
    
    var webURL: String = ""
    
    @IBOutlet weak var infoLabel1: UILabel!
    @IBOutlet weak var infoLabel2: UILabel!
    @IBOutlet weak var infoLabel3: UILabel!
    
    @IBOutlet weak var infoField1: UIButton!
    @IBOutlet weak var infoField2: UIButton!
    @IBOutlet weak var infoField3: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(adminBody)
        // Do any additional setup after loading the view.
        
        var foundFields = 0
        
        if let field1 = adminBody.electionInfoUrl {
            foundFields = foundFields + 1
            infoLabel1.text = "General Info: "
            infoField1.setTitle(field1, for: .normal)
        } else {
            infoLabel1.text = "No Additional Information Available"
            infoField1.isHidden = true
            
        }
        
        if let field2 = adminBody.absenteeVotingInfoUrl {
            foundFields = foundFields + 1
            if(foundFields == 1) {
                infoLabel1.text = "Absentee Voting Info: "
                infoField1.setTitle(field2, for: .normal)
            }
            else if(foundFields == 2){
                infoLabel2.text = "Absentee Voting Info: "
                infoField2.setTitle(field2, for: .normal)
            }
        }
        else {
            infoField2.isHidden = true
            
            infoLabel2.isHidden = true
            
        }
        
        if let field3 = adminBody.ballotInfoUrl {
            foundFields = foundFields + 1
            if(foundFields == 1) {
                infoLabel1.text = "Ballot Info: "
                infoField1.setTitle(field3, for: .normal)
            }
            else if(foundFields == 2){
                infoField2.isHidden = false
                
                infoLabel2.isHidden = false
                infoField3.isHidden = true
                
                infoLabel3.isHidden = true
                infoLabel2.text = "Ballot Info: "
                infoField2.setTitle(field3, for: .normal)
            }
            else {
                infoLabel3.text = "Ballot Info: "
                infoField3.setTitle(field3, for: .normal)
            }
        }
        else {
            
            infoField3.isHidden = true
            
            infoLabel3.isHidden = true
        }
        
        
    }
    
    @IBAction func but1Pressed(_ sender: Any) {
        webURL = infoField1.currentTitle ?? ""
        MyVariables.currentNewsSite = webURL
        self.performSegue(withIdentifier: "toInfoSite", sender: self)
    }
    
    @IBAction func but2Pressed(_ sender: Any) {
        webURL = infoField2.currentTitle ?? ""
        MyVariables.currentNewsSite = webURL
        self.performSegue(withIdentifier: "toInfoSite", sender: self)
    }
    
    @IBAction func but3Pressed(_ sender: Any) {
        webURL = infoField3.currentTitle ?? ""
        MyVariables.currentNewsSite = webURL
        self.performSegue(withIdentifier: "toInfoSite", sender: self)
    }
    
    
    // MARK: - Navigation
/*
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    
    }
 */

}
