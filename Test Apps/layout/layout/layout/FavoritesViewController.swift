//
//  FavoritesViewController.swift
//  layout
//
//  Created by Kristen Lau on 11/17/19.
//  Copyright © 2019 Sami Klein. All rights reserved.
//

import UIKit



class FavoritesCell: UITableViewCell {
    @IBOutlet weak var candidatePhoto: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var currentOffice: UILabel!
    @IBOutlet weak var runningFor: UILabel!
}

class FavoritesViewController: UIViewController, UITableViewDataSource {
    //https://www.ralfebert.de/ios-examples/uikit/uitableviewcontroller/custom-cells/
    
    @IBOutlet weak var favoritesTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        favoritesTable.dataSource = self
        
        
        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 12
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "theCell")! as! FavoritesCell
        cell.candidatePhoto?.image = UIImage(named: "obama")
        cell.name?.text = "Barack Obama"
        cell.currentOffice?.text = "President"
        cell.runningFor?.text = "Nothing"
        return cell
        
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
