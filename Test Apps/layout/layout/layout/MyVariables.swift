//
//  MyVariables.swift
//  layout
//
//  Created by Nicholas Bach on 11/20/19.
//  Copyright © 2019 Sami Klein. All rights reserved.
//

import Foundation

struct MyVariables{
    static var api_key: String = "&key=AIzaSyDR2SZV7sv5g5rcRPNjVFGo-CL6y-d_LhY"
//    "AIzaSyCFsugYR6EHOcbR_2Zqg6ZND9jYDurj9qU"
    
    static var address: String? = ""
    static var apiResults: api_results?
    static var elect_api_results: election_api_results?
    static var electionAddress: String = ""
    static var electionQuery: String = "https://www.googleapis.com/civicinfo/v2/voterinfo?electionId=2000&address="
    static var currentNewsSite: String = ""
}
