//
//  NewsViewController.swift
//  layout
//
//  Created by Sami Klein on 11/30/19.
//  Copyright © 2019 Sami Klein. All rights reserved.
//

import UIKit
import WebKit


class NewsViewController: UIViewController, WKUIDelegate {

    @IBOutlet weak var BBCbutton: UIButton!
    //    var webView: WKWebView!
    var websiteURL: String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    
    
    @IBAction func openBBC(_ sender: Any) {
        websiteURL = "https://www.bbc.com/news"
        MyVariables.currentNewsSite = websiteURL
        self.performSegue(withIdentifier: "toWebsite", sender: self)
    }
    
    
    @IBAction func openCNN(_ sender: Any) {
        websiteURL = "https://www.cnn.com/"
        MyVariables.currentNewsSite = websiteURL
        self.performSegue(withIdentifier: "toWebsite", sender: self)
    }
    
    
    @IBAction func openFox(_ sender: Any) {
        websiteURL = "https://www.foxnews.com/"
        MyVariables.currentNewsSite = websiteURL
        self.performSegue(withIdentifier: "toWebsite", sender: self)
    }
    
    
    @IBAction func openMSNBC(_ sender: Any) {
        websiteURL = "https://www.msnbc.com/"
        MyVariables.currentNewsSite = websiteURL
        self.performSegue(withIdentifier: "toWebsite", sender: self)
    }
    
    
    
    @IBAction func openNYT(_ sender: Any) {
        websiteURL = "https://www.nytimes.com/"
        MyVariables.currentNewsSite = websiteURL
        self.performSegue(withIdentifier: "toWebsite", sender: self)
    }
    
    
    @IBAction func openUSAtoday(_ sender: Any) {
        websiteURL = "https://www.usatoday.com/"
        MyVariables.currentNewsSite = websiteURL
        self.performSegue(withIdentifier: "toWebsite", sender: self)
    }

    
    
    @IBAction func openWashPost(_ sender: Any) {
        websiteURL = "https://www.washingtonpost.com/"
        MyVariables.currentNewsSite = websiteURL
        self.performSegue(withIdentifier: "toWebsite", sender: self)
        
    }
    
    
    
    // MARK: - Navigation
/*
     //In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
         //Get the new view controller using segue.destination.
         //Pass the selected object to the new view controller.
    }
 */
}
