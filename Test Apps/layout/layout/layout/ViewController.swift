//
//  ViewController.swift
//  layout
//
//  Created by Sami Klein on 11/16/19.
//  Copyright © 2019 Sami Klein. All rights reserved.
//

import UIKit
import MapKit


class ViewController: UIViewController, UISearchBarDelegate, CLLocationManagerDelegate {


    
    var locationManager = CLLocationManager()
    var userLocation: CLLocation?
    
    var newURLSearch: URL?
    var newURLAddress: URL?
    var searchAddress: String?
    
    
    var apiKey: String = "&key=AIzaSyDR2SZV7sv5g5rcRPNjVFGo-CL6y-d_LhY"
    
    var query: String = "https://www.googleapis.com/civicinfo/v2/representatives?address="
    var queryURL: URL?
    
    var searchSuccess = false
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var locationButton: UIButton!
    
    
    @IBOutlet weak var searchBar: UISearchBar!

    @IBOutlet weak var goButton: UIButton!
    
    @IBAction func goPress(_ sender: Any) {
    }
    
    var api_key: String = "AIzaSyDR2SZV7sv5g5rcRPNjVFGo-CL6y-d_LhY"
//    "AIzaSyCFsugYR6EHOcbR_2Zqg6ZND9jYDurj9qU"

    var address_query: String = "https://www.googleapis.com/civicinfo/v2/representatives?address="
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        searchBar.resignFirstResponder()
        searchAddress = searchBar.text!
        
        queryAPI()
        
        
//        }
//        else {
//            //address search not valid
//        }
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchBar.delegate = self
        goButton.isHidden = true
        titleLabel.text = "Welcome!"
        
        self.locationManager.requestWhenInUseAuthorization()
        
        if(CLLocationManager.locationServicesEnabled()){
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
            //self.performSegue(withIdentifier: "LoginToVotingInfo", sender: self)
        }
        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    func queryAPI(){
        self.queryURL = URL(string: query+searchAddress!.replacingOccurrences(of: " ", with: "+")+apiKey)
        DispatchQueue.global(qos: .userInitiated).async {
            do{
                print(try Data(contentsOf: self.queryURL!))
                let data = try! Data(contentsOf: self.queryURL!)
                MyVariables.apiResults = try! JSONDecoder().decode(api_results.self, from: data)
                
                
                let tempAddress = MyVariables.apiResults?.normalizedInput
                
                var errorMessage = "Address is:\n" + tempAddress!.line1
                errorMessage += "\n" + tempAddress!.city
                errorMessage +=  ", " + tempAddress!.state
                errorMessage += ", " + tempAddress!.zip
                errorMessage += "\nWould you like to continue?"
                
                MyVariables.electionAddress = tempAddress!.line1
                MyVariables.electionAddress += " " + tempAddress!.city
                MyVariables.electionAddress += " " + tempAddress!.state
                MyVariables.electionAddress += " " + tempAddress!.zip
                MyVariables.electionAddress = MyVariables.electionAddress.replacingOccurrences(of: " ", with: "%20")
                let electionURL = URL(string: "\(MyVariables.electionQuery)\(MyVariables.electionAddress)\(MyVariables.api_key)")
                
                let electData = try! Data(contentsOf: electionURL!)
                MyVariables.elect_api_results = try! JSONDecoder().decode(election_api_results.self, from: electData)
                
                let alert = UIAlertController(title: "Alert", message: errorMessage, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Proceed", style: .default, handler: { action in
                    switch action.style{
                    
                    
                    
                    
                    case .default:
                        MyVariables.address = self.searchAddress!.replacingOccurrences(of: " ", with: "+")
                        
                        //if self.searchSuccess == true {
                        //self.goButton.isHidden = false
                        
                        self.performSegue(withIdentifier: "LoginToVotingInfo", sender: self)
                        
                    case .cancel:
                        print("cancel")
                        
                    case .destructive:
                        print("destructive")
                        
                    default: break
                    }}))
                alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { action in
                    switch action.style{
                    case .default:
                        print("default")
                        
                    case .cancel:
                        print("cancel")
                        
                    case .destructive:
                        print("destructive")
                        
                        
                    default: break
                    }}))
                self.present(alert, animated: true, completion: nil)
                
                
            } catch {
                print("Error")
                let alert = UIAlertController(title: "Alert", message: "Error Finding Address. Please check again and try to be more specific", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                    switch action.style{
                    case .default:
                        print("default")
                        
                    case .cancel:
                        print("cancel")
                        
                    case .destructive:
                        print("destructive")
                        
                    default: break
                        
                    }}))
                self.present(alert, animated: true, completion: nil)
            }
            
        }
        
    }


    func lookUpCurrentLocation(completionHandler: @escaping (CLPlacemark?)
        -> Void ) {
        // Use the last reported location.
        if let lastLocation = userLocation {
            let geocoder = CLGeocoder()
            
            // Look up the location and pass it to the completion handler
            geocoder.reverseGeocodeLocation(lastLocation,
                                            completionHandler: { (placemarks, error) in
                if error == nil {
                    let firstLocation = placemarks?[0]
                    completionHandler(firstLocation)
                }
                else {
                    // An error occurred during geocoding.
                    completionHandler(nil)
                }
            })
        }
        else {
            // No location was available.
            completionHandler(nil)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let _:CLLocationCoordinate2D = manager.location!.coordinate
        
        userLocation = locations.last
    }

    @IBAction func UseCurrLocation(_ sender: Any) {
        var finalAddress = ""
        lookUpCurrentLocation { (placemark) in
            let currPlacemark = placemark
            
            var tempAddress = (currPlacemark?.name)!+" "
            tempAddress += (currPlacemark?.locality)!+" "
            tempAddress += (currPlacemark?.administrativeArea!)!+" "
            tempAddress += (currPlacemark?.postalCode!)!
            
            finalAddress = tempAddress
            
            MyVariables.address = finalAddress
            self.searchAddress = MyVariables.address
            self.searchBar.text = MyVariables.address
            self.queryAPI()
        }
        
    }
}

