//
//  ChoiceViewController.swift
//  layout
//
//  Created by Zachary Steinberg on 11/18/19.
//  Copyright © 2019 Sami Klein. All rights reserved.
//

import UIKit

class ChoiceViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let buttonRect = CGRect(x: view.center.x/2, y: view.center.y/2, width: view.bounds.width/2, height: 20)
        
        let repsButton = UIButton(frame: buttonRect)
        let electButton = UIButton(frame: buttonRect)
        
        repsButton.titleLabel?.text = "Current Reps"
        electButton.titleLabel?.text = "View Elections"
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
