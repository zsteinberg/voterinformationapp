//
//  CandidateWebsiteViewController.swift
//  layout
//
//  Created by Sami Klein on 11/16/19.
//  Copyright © 2019 Sami Klein. All rights reserved.
//

import UIKit
import WebKit

class CandidateWebsiteViewController: UIViewController, WKNavigationDelegate, WKUIDelegate {

    var url: URL?
    var savedRepName: String = ""
    var savedRepParty: String = ""
    var savedRepEmailAddress: String = ""
    var savedRepPhotoURL: URL?
    var savedRepPhone: String = ""
    
    @IBOutlet weak var candidateWebView: WKWebView!
    
    //https://developer.apple.com/documentation/webkit/wkwebview
//    override func loadView() {
//        let webConfiguration = WKWebViewConfiguration()
//        webView.navigationDelegate = self
//        webView.uiDelegate = self
//        view = webView
//    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        candidateWebView.navigationDelegate = self
        //let myLink = "https://www.apple.com" //PLACEHOLDER
        let myURL = url
        let myRequest = URLRequest(url: myURL!)
        candidateWebView.load(myRequest)
        candidateWebView.allowsBackForwardNavigationGestures = true
        
        self.navigationItem.hidesBackButton = true
                        let newBackButton = UIBarButtonItem(title: "Back", style: UIBarButtonItem.Style.bordered, target: self, action: "back:")
                        self.navigationItem.leftBarButtonItem = newBackButton
        
    }


    @IBAction func backButPressed(_ sender: Any) {
        self.presentingViewController?.dismiss(animated: true)
    }
    
     //MARK: - Navigation

     //In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     //    Get the new view controller using segue.destination.
     //    Pass the selected object to the new view controller.
        
        let SController = segue.source as! CandidateWebsiteViewController
        let DController = segue.destination as! DetailedCandidateViewController
        DController.webURL =  SController.url
        DController.repEmailAddress =  SController.savedRepEmailAddress
        DController.repName =  SController.savedRepName
        DController.repParty =  SController.savedRepParty
        DController.repPhone =  SController.savedRepPhone
        DController.repPhotoURL =  SController.savedRepPhotoURL
    }
 

}
