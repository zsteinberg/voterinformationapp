//
//  MainViewController.swift
//  layout
//
//  Created by Sami Klein on 11/16/19.
//  Copyright © 2019 Sami Klein. All rights reserved.
//

import UIKit

var contests: [Contest] = []

class ElectionsCell: UITableViewCell {
    
    @IBOutlet weak var ElectionName: UILabel!
    @IBOutlet weak var district: UILabel!
    
}

class MainViewController: UIViewController, UITableViewDataSource, UITableViewDelegate{
    
    
    
    @IBOutlet weak var localInfo: UIButton!
    @IBOutlet weak var stateInfo: UIButton!
    var addressFormatted: String?
    var elect_results: election_api_results?
    
    @IBOutlet weak var pollingInfoText: UITextView!
    @IBOutlet weak var electionTable: UITableView!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ElectionsCell")! as! ElectionsCell
        
        print("name: " + elect_results!.election.name)
        
        cell.ElectionName?.text = elect_results?.election.name
        
        return cell
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        electionTable.delegate = self
        electionTable.dataSource = self
        
        self.elect_results = MyVariables.elect_api_results
        
//        electionTable.delegate = self
//        electionTable.reloadData()
        
        var pollLocText = ""
        
        if (MyVariables.elect_api_results?.pollingLocations != nil) {
        pollLocText =  (MyVariables.elect_api_results?.pollingLocations![0].address.line1)!
        pollLocText += "\n" + (MyVariables.elect_api_results?.pollingLocations![0].address.city)!
        pollLocText +=  ", " + (MyVariables.elect_api_results?.pollingLocations![0].address.state)!
        pollLocText += ", " + (MyVariables.elect_api_results?.pollingLocations![0].address.zip)!
        }
        else {
            pollLocText = "Polling Location Unavailable for Current Address"
        }
        
        if MyVariables.elect_api_results?.state[0].electionAdministrationBody == nil {
            stateInfo.isHidden = true
        }
        
        if MyVariables.elect_api_results?.state[0].local_jurisdiction == nil {
            localInfo.isHidden = true
        }
        pollingInfoText.text = pollLocText
        
        
//        DispatchQueue.global(qos: .userInitiated).async {
//            do{
//
//
//
//                DispatchQueue.main.async {
//
//                    for contest in MyVariables.elect_api_results!.contests{
//                        contests.append(contest)
//                        self.contestTable.reloadData()
//                    }
//
//                }
//            }
//
//        }
        
        
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//
//    }
//
//    override func viewWillDisappear(_ animated: Bool) {
//
//    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
