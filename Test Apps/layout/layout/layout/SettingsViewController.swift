//
//  SettingsViewController.swift
//  layout
//
//  Created by Nicholas Bach on 11/18/19.
//  Copyright © 2019 Sami Klein. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {

    @IBOutlet weak var changeLocationButton: UIButton!
    
    
    @IBAction func changeDarkMode(_ sender: Any) {
        view.backgroundColor = UIColor(named: "customColorSet")
    }
    
    @IBOutlet weak var fireworksImage: UIImageView!
    
    @IBOutlet weak var iVotedPic: UIButton!
    
    @IBAction func iVotedPressed(_ sender: Any) {
        
//        iVotedPic.bringSubviewToFront(fireworksImage)
//        fireworksImage.bringSubviewToFront(iVotedPic)
        fireworksImage.layer.zPosition = -5
        iVotedPic.layer.zPosition = 5

        fireworksImage.isHidden = false
        
//        UIView.animate(withDuration: 2.0, animations: {
//            self.fireworksImage.transform = CGAffineTransform(scaleX: 2, y: 2)
//        })
//        UIView.animate(withDuration: 2.0, animations: {
//            self.fireworksImage.transform = .identity
//        })
        
        UIView.animate(withDuration: 2.0, animations: {
//            self.ivotedpic.transform =
            self.iVotedPic.imageView!.transform = CGAffineTransform(rotationAngle: (180.0 * .pi) / 180.0)
        })
        UIView.animate(withDuration: 2.0, animations: {
            self.iVotedPic.imageView!.transform = .identity
        })
//        fireworksImage.isHidden = true
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
 
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
