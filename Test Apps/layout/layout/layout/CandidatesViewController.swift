//
//  CandidatesViewController.swift
//  layout
//
//  Created by Kristen Lau on 11/17/19.
//  Copyright © 2019 Sami Klein. All rights reserved.
//

import UIKit

class CandidateCell: UITableViewCell {

    @IBOutlet weak var candidateImage: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var currentOffice: UILabel!
    @IBOutlet weak var runningFor: UILabel!
    @IBOutlet weak var label: UILabel!
}



class CandidatesViewController: UIViewController, UITableViewDataSource {
    @IBOutlet weak var contestName: UILabel!
    var contest: Contest?
    var number: Int?
    
    @IBOutlet weak var candidatesTable: UITableView!
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (contest?.candidates!.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "candidateCell")! as! CandidateCell
      
        
        let cand = contest!.candidates![indexPath.row]
        cell.label?.text = nil
        
        if cand.party != nil {
            if cand.party == "Democratic"{
                cell.candidateImage?.image = UIImage(named: "democrat")
            }
            else if cand.party == "Republican" {
                cell.candidateImage?.image = UIImage(named: "republican")
            }
            else if cand.party == "Green" {
                cell.candidateImage?.image = UIImage(named: "green")
            }
            else if cand.party == "Libertarian" {
                cell.candidateImage?.image = UIImage(named: "libertarian")
            }
            else if cand.party == "Nonpartisan" {
                cell.candidateImage?.image = UIImage(named: "nonpartisan")
            }
            else {
                cell.candidateImage?.image = UIImage(named: "NoImageAvail")
            }
        }
        cell.candidateImage.contentMode = .scaleAspectFit
        cell.name?.text = cand.name
        cell.currentOffice?.text = "Party: \(cand.party!)"
        cell.runningFor?.text = "Running For: \(contest!.office!)"
        
        
        
        return cell
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        candidatesTable.dataSource = self
        if contest!.type != "Referendum" {
            if (contest!.office! != nil){
                contestName.text = "Candidates for \(contest!.office!)"
            }
            else{
                contestName.text = "Error"
            }
        }
        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
