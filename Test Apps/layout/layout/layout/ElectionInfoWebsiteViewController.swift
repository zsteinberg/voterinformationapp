//
//  ElectionInfoWebsiteViewController.swift
//  layout
//
//  Created by Zachary Steinberg on 12/2/19.
//  Copyright © 2019 Sami Klein. All rights reserved.
//

import UIKit
import WebKit

class ElectionInfoWebsiteViewController: UIViewController, WKNavigationDelegate, WKUIDelegate {
    
    var myUrl = ""
    

    @IBOutlet weak var webView: WKWebView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        webView.navigationDelegate = self
        let url = URL(string: MyVariables.currentNewsSite)!
        let myURLrequest = URLRequest(url: url)
        webView.load(myURLrequest)
        webView.allowsBackForwardNavigationGestures = true
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func backButPressed(_ sender: Any) {
                self.presentingViewController?.dismiss(animated: true)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
