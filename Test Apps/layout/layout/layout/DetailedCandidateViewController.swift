//
//  DetailedCandidateViewController.swift
//  layout
//
//  Created by Kristen Lau on 11/18/19.
//  Copyright © 2019 Sami Klein. All rights reserved.
//

import UIKit

class DetailedCandidateViewController: UIViewController {

    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var party: UILabel!
    @IBOutlet weak var emailAddress: UILabel!
    @IBOutlet weak var phone: UILabel!
    @IBOutlet weak var website: UIButton!
    @IBOutlet weak var favorite: UIButton!
    @IBOutlet weak var back: UIButton!
    
    
    @IBOutlet weak var redField: UITextView!
    
    //@IBOutlet weak var title: UILabel!
    
    
    var webURL: URL?
    var rep: Officials?
    var repName: String = ""
    var repParty: String = ""
    var repEmailAddress: String = ""
    var repPhotoURL: URL?
    var repPhone: String = ""
    var cameFromCurrentReps = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        populateInfo()
        
        view.bringSubviewToFront(name)
        view.bringSubviewToFront(back)
//        view.bringSubviewToFront(favorite)

        // Do any additional setup after loading the view.
    }
    
    
    func populateInfo() {
        if repPhotoURL != nil {
            let data = try? Data(contentsOf: repPhotoURL!)
            image?.image = UIImage(data: data!)
        } else {
            let noPosterAvail = #imageLiteral(resourceName: "NoImageAvail")
            image?.image = noPosterAvail
        }
        
       
        
        name?.text = repName
        party?.text = repParty
        emailAddress?.text = repEmailAddress
        phone?.text = repPhone
        print("rep party = " + repParty)
        if (repParty != "Republican Party"){
            redField.backgroundColor = UIColor.blue
        }
        
        if webURL != nil{
            let webtext = name?.text ?? "No Name"
            website.setTitle(webtext + " Website", for: .normal)
        } else {
            website.isHidden = true
        }

    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
    }


    
    @IBAction func goBack(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.

        let SController = segue.source as! DetailedCandidateViewController
        let DController = segue.destination as! CandidateWebsiteViewController

        DController.url =  SController.webURL
        DController.savedRepEmailAddress =  SController.repEmailAddress
        DController.savedRepName =  SController.repName
        DController.savedRepParty =  SController.repParty
        DController.savedRepPhone =  SController.repPhone
        DController.savedRepPhotoURL =  SController.repPhotoURL

        
    }
 

}
