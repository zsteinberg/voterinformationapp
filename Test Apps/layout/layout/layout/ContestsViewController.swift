//
//  ContestsViewController.swift
//  layout
//
//  Created by Kristen Lau on 11/29/19.
//  Copyright © 2019 Sami Klein. All rights reserved.
//

import UIKit



class ContestsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var ContestsTable: UITableView!
    var results: election_api_results = MyVariables.elect_api_results!
    var contests: [Contest]?
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return MyVariables.elect_api_results!.contests.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "contestCell")! as! ElectionsCell
        if MyVariables.elect_api_results!.contests[indexPath.row].district != nil{
            let district = MyVariables.elect_api_results!.contests[indexPath.row].district!
            if district.scope == "statewide" {
                cell.district.text = "State Wide"
            }
            else if district.scope == "countywide" {
                cell.district.text = "County Wide"
            }
            else {
                cell.district.isHidden = true
            }
        }
        if MyVariables.elect_api_results!.contests[indexPath.row].type == "Referendum"{
            cell.ElectionName.text = MyVariables.elect_api_results!.contests[indexPath.row].referendumTitle!
        }
        else{
            if MyVariables.elect_api_results!.contests[indexPath.row].office != nil{
                cell.ElectionName.text = MyVariables.elect_api_results!.contests[indexPath.row].office!
            }
            else {
                cell.ElectionName.text = "Not Listed"
            }
        }
        
        return cell
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        //print(MyVariables.elect_api_results!.contests)
        contests = MyVariables.elect_api_results!.contests
        ContestsTable.delegate = self
        ContestsTable.dataSource = self
        ContestsTable.reloadData()

        // Do any additional setup after loading the view.
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let nextContest = CandidatesViewController()
//        var indexpath = indexPath.row.info
        
//        detailedVC.image = theImageCache[indexPath.row]
//        detailedVC.imageName = theData[indexPath.row].name
        if MyVariables.elect_api_results!.contests[indexPath.row].type != "Referendum"{
            let contest = MyVariables.elect_api_results!.contests[indexPath.row]
            
            let nextController = (self.storyboard?.instantiateViewController(withIdentifier: "candidatesPage") as? CandidatesViewController)!
            nextController.contest = contest
            nextController.number = indexPath.row
            self.show(nextController, sender: self.storyboard)
        }
        
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
