//
//  jurisdiction.swift
//  Elections
//
//  Created by Zachary Steinberg on 11/13/19.
//  Copyright © 2019 Zachary Steinberg. All rights reserved.
//

import Foundation

struct jurisdiction: Codable {
    let name: String
    let electionAdministrationBody: adminBody?
}
