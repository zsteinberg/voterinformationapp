//
//  official.swift
//  Elections
//
//  Created by Zachary Steinberg on 11/13/19.
//  Copyright © 2019 Zachary Steinberg. All rights reserved.
//

import Foundation

struct Official: Codable {
    let name: String?
    let title: String?
    let officePhoneNumber: String
    let faxNumber: String?
    let emailAddress: String?
}
