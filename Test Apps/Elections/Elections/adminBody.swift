//
//  adminBody.swift
//  Elections
//
//  Created by Zachary Steinberg on 11/13/19.
//  Copyright © 2019 Zachary Steinberg. All rights reserved.
//

import Foundation

struct adminBody: Codable {
    let name: String?
    let electionInfoUrl: String?
    let electionRegistrationUrl: String?
    let electionRegistrationConfirmationUrl: String?
    let absenteeVotingInfoUrl: String?
    let votingLocationFinderUrl: String?
    let ballotInfoUrl: String?
    let electionRulesUrl: String?
    let hoursOfOperation: String?
    let correspondenceAddress: normAddress?
    let physicalAddress: normAddress?
    let electionOfficials: [Official]?
}
