//
//  ViewController.swift
//  Elections
//
//  Created by Zachary Steinberg on 11/12/19.
//  Copyright © 2019 Zachary Steinberg. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UISearchBarDelegate {

    @IBAction func queryButton(_ sender: Any) {
        setAPI()
    }
    @IBOutlet weak var searchBar: UISearchBar!
    var query: String = "https://www.googleapis.com/civicinfo/v2/voterinfo?key=AIzaSyDR2SZV7sv5g5rcRPNjVFGo-CL6y-d_LhY&address=11023%20Earlsgate%20Ln%20Rockville%20MD%2020852&electionId=2000"
//    address=735%20Interdrive%20MO%2063130
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        searchBar.delegate = self
    }
    
    func setAPI(){
        DispatchQueue.global(qos: .userInitiated).async {
            let url = URL(string: self.query)
            let data = try! Data(contentsOf: url!)
            let json = try! JSONDecoder().decode(api_results.self, from: data)
            print(json)
        }
        
    }
}

