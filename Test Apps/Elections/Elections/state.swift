//
//  State.swift
//  Elections
//
//  Created by Zachary Steinberg on 11/13/19.
//  Copyright © 2019 Zachary Steinberg. All rights reserved.
//

import Foundation

struct State: Codable {
    let name: String
    let electionAdministrationBody: adminBody
    let local_jurisdiction: jurisdiction
    let sources: [Source]
}
